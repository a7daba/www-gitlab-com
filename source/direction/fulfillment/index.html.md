---
layout: markdown_page
title: Product Vision - Fulfillment
---

## On this page
{:.no_toc}

- TOC
{:toc}

_"When technology delivers basic needs, user experience dominates."_  - Donald Norman

This is the product vision for Fulfillment. Fulfillment manages a variety of categories that are important for GitLab's ongoing success. Right now, these categories include Licensing and Transactions.

If you'd like to discuss this vision directly with the Product Manager for Fulfillment,
feel free to reach out to Luca Williams via [e-mail](mailto:luca@gitlab.com),
[Twitter](https://twitter.com/tipyn2903), or by [scheduling a video call](https://calendly.com/tipyn).

| Category | Description |
| ------ |  ------ |
| [🎁 Transactions](/direction/fulfillment/transactions) | How our users purchase GitLab Plans and add-ons, and how they manage their account information. |
| [💌 Licensing](/direction/fulfillment/licensing) | How we fulfill our users purchases of GitLab Plans through license key distribution. |

## ⛵️ Overview

The overall vision for Fulfillment is to provide GitLab’s customers and GitLab  with an invisible and user experience when handling their license keys and managing their purchases. 

Billing, account management and handling license keys can be confusing, boring and time-consuming. We want these things to be easy and frictionless so you can focus on enjoying our product. In addition to our users, we also prioritise the experience GitLab Team Members (and Resellers). In order for our GitLab Team Members and Resellers to provide a great service to our users, the administration and 'behind-the-scenes' user experience of our billing and licensing services are held to the same standard as the experience of our users.

Due to the nature of the areas covered by Fulfillment and the fact that the group is repsonsible for collecting and storing personal and billing information, We also aim to ensure that Fulfillment's services meet all compliance, security and privacy requirements for our users.

### 🚀 Transactions Priorities

#### 🔥 Current focus

| Priority | Focus | Why? |
| :------: | ------ | ------ |
| 1️⃣ | [🎊 Self-service upgrades for GitLab.com (SaaS)](https://gitlab.com/groups/gitlab-org/-/epics/926) | This is a first iteration towards enabling our customers to [fully serve themselves](https://gitlab.com/groups/gitlab-org/-/epics/923) when needing to perform any billing tasks. We recieve a [significant number of requests](https://gitlab.com/gitlab-org/customers-gitlab-com/issues/521) per week from our users who would like to upgrade a Plan so implementing this would not only reduce workload on GitLab Team Members, freeing them up to help users in other ways, but it would also improve the overall user experience and potentially reduce churn. |

#### 🎉 Next up

| Priority | Focus | Why? |
| :------: | ------ | ------ |
| 2️⃣ | [An improved free trial sign-up experience for GitLab.com (SaaS) users](https://gitlab.com/gitlab-org/gitlab-ce/issues/6350) | A free trial is often the first interaction our users have with our Product and therefore could be a dealbreaker for them. Currently, the free trial experience as a whole is confusing,lengthy and frustrating and this issue to improve the sign-up flow of our GitLab.com free trial is our first iteration towards creating a delightful and exciting first-look experience into GitLab as a product. |
| 3️⃣ | [An improved purchase experience for GitLab.com (SaaS) users](https://gitlab.com/gitlab-org/gitlab-ce/issues/63600) | Similarly to the free trial experience, our purchasing flow needs a serious refresh and this issue aims to significantly reduce the friction and frustration for our GitLab.com users who need to do business with us. |
| 4️⃣ | [Automatically bill for newly added group members on GitLab.com](https://gitlab.com/gitlab-org/customers-gitlab-com/issues/168) | One of our goals in Fulfillment is to create a fair billing experience for our users and this issue is an iteration towards that that goal. By implementing this, we can begin to shape a multi-directional billing flow that ensures our customers are fairly billed for what they use, and GitLab as a company accurately recieves payment for the services provided. | 

### 🚀 Licensing Priorities

#### 🔥 Current focus

| Priority | Focus | Why? |
| :------: | ------ | ------ |
| 1️⃣ | [Allow users to download their license key from the transactions portal](https://gitlab.com/gitlab-org/customers-gitlab-com/issues/281) | This issue is important as a small but significant step towards unifying our Self-Managed customers billing and licensing experience. Currently the only way for folks to access their license key is to have a GitLab Team Member email it to them - this issue removes a considerable amount of friction around that process and allows users to have more control over their own license key management. |

#### 🎉 Next up

| Priority | Focus | Why? |
| :------: | ------ | ------ |
| 2️⃣ | [Maximum Users should always reflect current counts](https://gitlab.com/gitlab-org/gitlab-ee/issues/8418) | True-ups are a huge friction point for many companies and GitLab is not unique in this regard. This issue aims to provide more clarity to both users and GitLab Team Members who are helping customers as to what the true maximum user count is on any customer instance which will aid a more seamless and accurate billing experience for both sides.  |


## How we prioritize

We follow the same prioritization guidelines as the [product team at large](https://about.gitlab.com/handbook/product/#prioritization). Issues tend to flow from having no milestone, to being added to the backlog, to a directional milestone (e.g. Next 3-4 releases), and are finally assigned a specific milestone.

Our entire public backlog for Fulfillment can be viewed [on the issue board](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Afulfillment), and can be filtered by labels or milestones. If you find something you are interested in, you're encouraged to jump into the conversation and participate. At GitLab, everyone can contribute!

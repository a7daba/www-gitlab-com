---
layout: markdown_page
title: "Process for Modifying Sales Templates"
---
## On this page
{:.no_toc}
- TOC
{:toc}

The process below defines the Conga Conga Contracts workflow within Salesforce for initiating contract negotiations of GitLab's standard templates once Sales has notified the Legal team that a customer wishes to receive an editable version of one of our templates. 

## Generate Contract 
1. Press “Select Clause Bundle” 
2. Choose applicable bundle (Subscription or Service) 
3. Select “Save” 
4. When returned to the newly created contract record, press “Generate Contracts” button 
5. Select applicable contract type  
6. Press “Merge and Download” (Note: A local copy of the template will be created but it will be automatically saved to the Contract Object so there is no need to save this file) 

## Send Contract for Negotiations 
1. On the contract record, press the “Select Primary Document” button 
2. Choose the Primary Document (This will be the original sent out for negotiation) and press “Save” 
3. Press the “Send for Negotiation” button 
4. Fill in Recipient information provided by Sales with a copy to the sales requestor
5. In the "Subject" line type "(Updated/Final or neither) GitLab Subscription (or Service) Agreement"
6. Press Send

## Finalize the Contract Record and Notify the Salesperson 
1. Make sure that the final, negotiated contract is in the contract record. (If any negotiations have happened outside of SalesForce/Conga, upload the final, agreed-upon version to the contract record as a new version of the contract.)
2. Change the Contract Status to “Approved to Sign”.
3. Send a chatter message to the Sales account owner to stage for signing.
4. Once signed, the sales account owner will be responsible for uploading the final document, filling out all of the remaining fields in the contract record, and changing the Contract Status to "Active"

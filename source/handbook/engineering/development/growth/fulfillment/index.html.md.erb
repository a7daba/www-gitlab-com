---
layout: markdown_page
title: Fulfillment Team
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Vision

For more details about the product vision for Fulfillment, see our [Fulfillment](/direction/fulfillment/) page.

Fulfillment manages several product categories:

| Category | Description |
| ------ |  ------ |
| [Licensing (license.gitlab.com)](https://gitlab.com/gitlab-org/license-gitlab-com) | Covers all aspects of our licensing model, from how we count seats and conduct true-up to how we count active seats and keep the customer informed on their seat utilization. |
| [Transactions (customers.gitlab.com)](https://gitlab.com/gitlab-org/customers-gitlab-com) | How customers pay for GitLab. Licensing is about how we package GitLab as an offering, whereas Transactions is about how we fulfill those business relationships and how we make doing business with GitLab a great experience for both self-managed and GitLab.com. |
| [Telemetry (usage statistics, version check)](https://gitlab.com/gitlab-org/version-gitlab-com) | Help instances understand more about how people are using GitLab. |

### Team members

<%= direct_team(manager_role: 'Engineering Manager, Fulfillment (Interim)') %>

### Stable counterparts

<%= stable_counterparts(role_regexp: /[,&] Fulfillment/, direct_manager_role: 'Engineering Manager, Fulfillment (Interim)') %>

### How we work

* In accordance with our [GitLab values](https://about.gitlab.com/handbook/values/)
* Transparently: nearly everything is public, we record/livestream meetings whenever possible
* We get a chance to work on the things we want to work on
* Everyone can contribute; no silos

#### Development workflow

```mermaid
graph TD
    A(Planning and prioritization) -->|Estimation | B[Ready for development]
    B --> C[In dev]
    C --> D[In review]
    D --> E(Closed fa:fa-check-circle)
```

#### Planning

We plan in monthly cycles in accordance with our [Product Development Timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline).
Release scope for an upcoming release should be finalized by the `1st`.

On or around the `26th`: Product meets with Engineering Managers for a preliminary issue review. Issues are tagged with a milestone and are estimated initially.

#### Prioritization

Our planning process is reflected in boards that should always reflect our current priorities. You can see our priorities for [backend](https://gitlab.com/groups/gitlab-org/-/boards/1072592).

Priorities should be reflected in the priority label for scheduled issues:

| Priority | Description | Probability of shipping in milestone |
| ------ | ------ | ------ |
| P1 | **Urgent**: top priority for achieving in the given milestone. These issues are the most important goals for a release and should be worked on first; some may be time-critical or unblock dependencies. | ~100% |
| P2 | **High**: important issues that have significant positive impact to the business or technical debt. Important, but not time-critical or blocking others.  | ~75% |
| P3 | **Normal**: incremental improvements to existing features. These are important iterations, but deemed non-critical. | ~50% |
| P4 | **Low**: stretch issues that are acceptable to postpone into a future release. | ~25% |

You can read more about prioritization on the [direction page](/direction/fulfillment#how-we-prioritize).

#### Estimation

Before work can begin on an issue, we should estimate it first after a preliminary investigation. This is normally done in the monthly planning meeting.

When estimating development work, please assign an issue an appropriate weight:

| Weight | Description (Engineering) |
| ------ | ------ |
| 1 | The simplest possible change. We are confident there will be no side effects. |
| 2 | A simple change (minimal code changes), where we understand all of the requirements. |
| 3 | A simple change, but the code footprint is bigger (e.g. lots of different files, or tests effected). The requirements are clear. |
| 5 | A more complex change that will impact multiple areas of the codebase, there may also be some refactoring involved. Requirements are understood but you feel there are likely to be some gaps along the way. |
| 8 | A complex change, that will involve much of the codebase or will require lots of input from others to determine the requirements. |
| 13 | A significant change that may have dependencies (other teams or third-parties) and we likely still don't understand all of the requirements. It's unlikely we would commit to this in a milestone, and the preference would be to further clarify requirements and/or break in to smaller Issues. |

Issues that may take longer than 2 days or have a weight of 3 or more, may require additional splitting (either in smaller issues if the requirements can be simplified, or by creating separate MRs for the same issue).

This will ensure that we can reach 10 MRs per dev per month, therefore increasing [throughput](/handbook/engineering/management/throughput/).

#### During the release

If a developer has completed work on an issue, they may open the ([prioritization board](https://gitlab.com/groups/gitlab-org/-/boards/1072592).
and begin working on the next prioritized issue.

Issues not already in development should be worked on in priority order.

An issue will have 3 stages, and they should be moved accordingly using the [Fulfillment workflow board](https://gitlab.com/groups/gitlab-org/-/boards/1072626?)

These are:

* Ready for development
* In dev
* In Review

#### Retrospectives

After the `8th`, the Fulfillment team conducts an [asynchronous retrospective](https://about.gitlab.com/handbook/engineering/management/team-retrospectives/). You can find current and past retrospectives for Fulfillment in [https://gitlab.com/gl-retrospectives/fulfillment/issues/](https://gitlab.com/gl-retrospectives/fulfillment/issues/).

## Common links

 * [All open Fulfillment epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Afulfillment)
 * [Issue Tracker](https://gitlab.com/gitlab-org/fulfillment/issues)
 * [Slack channel #g_fulfillment](https://gitlab.slack.com/app_redirect?channel=g_fulfillment)
 * [Dialy standup channel #g_fulfillment_daily](https://gitlab.slack.com/app_redirect?channel=g_fulfillment_daily)

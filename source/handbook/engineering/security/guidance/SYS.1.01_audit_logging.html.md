---
layout: markdown_page
title: "SYS.1.01 - Audit Logging Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# SYS.1.01 - Audit Logging

## Control Statement

GitLab logs critical information system activity.

## Context

Logging is the foundation for a variety of other security controls including monitoring, incident response, and configuration management. Without comprehensive and reliable logs, large parts of our security compliance program wouldn't be possible. This control is left vague by design. As we develop our system maps and inventories this control will likely become a bit more targeted. To start we really want all GitLab teams to enable system-level logging on all production systems.

## Scope

This logging control applies to all production systems.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SYS.1.01_audit_logging.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SYS.1.01_audit_logging.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SYS.1.01_audit_logging.md).

## Framework Mapping

* ISO
  * A.12.4.1
* SOC2 CC
  * CC7.2

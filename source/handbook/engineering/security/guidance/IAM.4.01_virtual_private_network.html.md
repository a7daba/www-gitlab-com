---
layout: markdown_page
title: "IAM.4.01 - Virtual Private Network Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# IAM.4.01 - Virtual Private Network

## Control Statement

Remote connections to the corporate network are accessed via VPN through managed gateways.

## Context

Where and only if applicable, appropriate, and technically feasible, and with the understanding GitLab is a cloud-native, fully-remote and international organization favoring a Zero Trust network without a traditional corporate network infrastructure, access will be done via a VPN through managed gateways.

## Scope

This control applies to all corporate networks.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IAM.4.01_virtual_private_network.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IAM.4.01_virtual_private_network.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IAM.4.01_virtual_private_network.md).

## Framework Mapping

* ISO
  * A.11.2.6
* SOC2 CC
  * CC6.6
  * CC6.7
* PCI
  * 8.1.1
  * 8.6

---
layout: markdown_page
title: "Debugging Failing tests"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Scheduled QA Test Pipelines
* [Nightly pipeline](https://gitlab.com/gitlab-org/quality/nightly/pipelines): Tests are run [daily at 4:00 am UTC](https://gitlab.com/gitlab-org/quality/nightly/pipeline_schedules/9530/edit) and results are reported on the [#qa-nightly](https://gitlab.slack.com/messages/CGLMP1G7M) slack channel.
* [Staging pipeline](https://gitlab.com/gitlab-org/quality/staging/pipelines): Tests are run [daily at 4:00 am UTC](https://gitlab.com/gitlab-org/quality/staging/pipeline_schedules/9514/edit) and results are reported on the [#qa-staging](https://gitlab.slack.com/messages/CBS3YKMGD) slack channel.

### Steps for Debugging QA Pipeline Test Failures

#### 1. Create an issue
  - For nightly failures, create an issue in: [https://gitlab.com/gitlab-org/quality/nightly/issues](https://gitlab.com/gitlab-org/quality/nightly/issues)
  - For staging failures, create an issue in: [https://gitlab.com/gitlab-org/quality/staging/issues](https://gitlab.com/gitlab-org/quality/staging/issues)
  - The issue description **should** have a link to the failing job, the stack trace from the job's logs, screenshot (if available) and HTML capture (if available).
  - The issue description **can** have a brief description of what you think is the cause of the failure and whether you were able to reproduce it on your local. In case it is reproducible, sometimes a video is also helpful.
  - The issue **should** have the following labels:
    - ~"Quality"
    - ~"bug"
    - a dev stage label ( ~"Create", ~"Manage", etc.)
    - ~"S1"
    - a priority label (one of ~"P1" and ~"P2" based on [priorities mentioned here.](/handbook/engineering/quality/guidelines#priorities))
  - The issue **should** have the current milestone if ~"P1" or the next milestone if ~"P2"
  - The issue **should** have a due date of current release feature freeze date if ~"P1" or the next release feature freeze date if ~"P2"

#### 2. Quarantine the test
  - Tests should be quarantined as soon as they start failing. Assign the `:quarantine` metadata to the test and also add a link to the issue.
  If the example has a `before` hook, the `:quarantine` meta should be assigned to the outer context to avoid running the `before` hook.  
  - The merge request **should** have the following labels:
    - ~"Quality"
    - ~"bug"
    - ~"Pick into xx.x" where the `xx.x` is the [next version to be released](https://about.gitlab.com/releases/) (*only* if we are in [feature freeze](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/PROCESS.md#feature-freeze-on-the-7th-for-the-release-on-the-22nd))
  - The merge request **can** have the following labels:
    - a dev stage label ( ~"Create", ~"Manage", etc.)
    - ~"Quality:flaky-tests" if you know for sure the failure is due to flakiness
  - The merge request **should** have the current milestone
  Here is an [example quarantine merge request](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/25631/diffs).    

#### 3. Investigate the failure
  - Understand the intent of the test. Manually performing the test steps can help.  
  - Stack trace: The stack trace shown in the job's log is the starting point for investigating the test failure.
  - Screenshots and HTML Captures: These are available for download in the job's artifact for up to 1 week after the job run.
  - QA Logs: These are also included in the job's artifact and are valuable for determining the steps taken by the tests before failing.     
  - Reproducing the failure locally:
    - You can try running the test against your local GitLab instance to see if the failure is reproducible. E.g.:

    `CHROME_HEADLESS=false bundle exec bin/qa Test::Instance::All http://localhost:3000 qa/specs/features/browser_ui/1_manage/project/create_project_spec.rb`  
    - Use the environment variable `QA_DEBUG=true` to enable logging output including page actions and Git commands.
    - You can also use the same docker image (same sha256 hash) as the one used in the failing job to run GitLab in a container on your local.
    In the logs of the failing job, search for `Downloaded newer image for gitlab/gitlab-ce:nightly` or `Downloaded newer image for gitlab/gitlab-ee:nightly`
    and use the sha256 hash just above that line.
    To run GitLab in a container on your local, the docker command similar to the one shown in the logs can be used. E.g.:

    `docker run --publish 80:80 --name gitlab --net test --hostname localhost gitlab/gitlab-ce:nightly@sha256:<hash>`
    - You can now run the test against this docker instance. E.g.:

    `CHROME_HEADLESS=false bundle exec bin/qa Test::Instance::All http://localhost qa/specs/features/browser_ui/1_manage/project/create_project_spec.rb`
    - Additional information about running tests locally can be found in the [QA readme](https://gitlab.com/gitlab-org/gitlab-ce/tree/master/qa#running-specific-tests).

  - Determine if the test is [flaky](https://docs.gitlab.com/ee/development/testing_guide/flaky_tests.html#whats-a-flaky-test): check the logs or run the test a few times. If it passes at least once but fails otherwise, it's flaky.

#### 4. Next Steps
  - In all of the cases below, any merge requests created **should** have the following labels:
    - ~"Quality"
    - ~"bug"
    - ~"Pick into xx.x" where the `xx.x` is the [next version to be released](https://about.gitlab.com/releases/) (*only* if we are in [feature freeze](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/PROCESS.md#feature-freeze-on-the-7th-for-the-release-on-the-22nd))
  - The merge request **can** have the following labels:
    - a dev stage label ( ~"Create", ~"Manage", etc.)
    - ~"Quality:flaky-tests" if you know for sure the failure is due to flakiness
  - The merge request **should** have the current milestone

##### If you are unable to reproduce the issue:
{:.no_toc}
  - If you are unable to reproduce the failure or if you think that the failure is due to a valid bug, refer the issue to the frontend and/or backend engineering manager of the concerned team by mentioning them in the issue comments.
    - To find the appropriate team member, please refer to the [Organizational Chart](/company/team/org-chart).

##### If you think it's a flaky test:
  - If you think the failure is due to flakiness in the test itself, please refer the issue to the concerned member of the [Quality Engineering team](/handbook/engineering/quality/#quality-engineering-teams).
  - If you decide to proceed with de-flaking the test, assign the issue to yourself.
  - Do not remove the test from quarantine in the same merge request as the test fix. Do not close the issue with the fix merge request. Flaky tests should stay in quarantine until proven stable.
  - Once fixed, create a new merge request to remove the test from quarantine and remove the issue link in the test. This merge request should also close the issue.
  - If the test was flaky, then you should confirm that the test is stable by passing at least 5 times before the merge request removing it from quarantine can be merged.
    - Note: the number of passes needed to be sure a test is stable is just a suggestion. You can use your judgement to pick a different threshold.

##### If you can fix this test, and it's not flaky (just broken):
  - If you decide to proceed with fixing the test, assign the issue to yourself.
  - It is ok to fix a test, remove it from quarantine, and close the issue all in the same merge request so long as the test was broken and not flaky.

#### Training Videos
Two videos walking through the triage process were recorded and uploaded to the [GitLab Unfilitered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) YouTube channel.
  - [Quality Team: Failure Triage Training - Part 1](https://www.youtube.com/watch?v=Fx1DeWoTG4M)
    - Covers the basics of investigating pipeline failures locally.
  - [Quality Team: Failure Triage Training - Part 2](https://www.youtube.com/watch?v=WeQb8GEw6PM)
    - Continued discussion with a focus on using Docker containers that were used in the pipeline that failed.

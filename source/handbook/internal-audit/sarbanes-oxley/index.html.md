---
layout: markdown_page
title: "Sarbanes-Oxley (SOX) Compliance"
---


## On this page
{:.no_toc}

- TOC
{:toc}

# Sarbanes-Oxley Act of 2002
[ Sarbanes Oxley Act 2002 ](https://en.wikipedia.org/wiki/Sarbanes%E2%80%93Oxley_Act)is a federal law that established auditing and financial regulations for financial reporting of public companies. This law was passed to increase transparency in  financial reporting by corporations and to require a formalized system of checks and balances in each company, thereby helping protect investors from fraudulent financial reporting. SOX applies to all publicly traded companies in the United States as well as wholly-owned subsidiaries and foreign companies that are publicly traded and do business in the United States. 

In order to build the confidence of the investors, the SOX regulations require the following:

*	CEOs and CFOs are directly responsible for the accuracy, documentation, and submission of all financial reports as well as the internal control structure to the [ SEC ](https://www.sec.gov/). 
*	Internal Control Report must state that management is responsible for an adequate internal control structure for their financial records. Any shortcomings must be reported up the chain as quickly as possible for transparency.
*	Companies should develop and implement a comprehensive data security strategy that protects and secures all financial data stored and utilized during normal operations.
*	Companies must maintain and provide documentation proving they are compliant and that they are continuously monitoring and measuring SOX compliance objectives.
*	External auditor must independently assess and certify the adequacy of controls over all known risks for the financial reporting. 

Formal penalties for non-compliance with SOX can include fines, removal from listings on public stock exchanges and invalidation of [ D&O ](https://en.wikipedia.org/wiki/Directors_and_officers_liability_insurance) policies. Under the Act, CEOs and CFOs who wilfully submit an incorrect certification to a SOX compliance audit can face fines and/or imprisonment.


<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/wZ8xDBgMat8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

# SOX Compliance Roadmap for Gitlab 

As Gitlab is planning to go public in November 2020, it is important that we prepare to comply with SOX ahead of listing, as the consequences of non-compliance can be severe.  Below are some of the important requirements we will need to adhere to: 
<div align="center">
    <img src="/images/handbook/internal-audit/sarbanes-oxley_section.jpg" align="center" height="400" width="600" >
</div>    



Points to note:
*	In the first year, an “exemption” is allowed for a new public company on [ 10-K ](https://en.wikipedia.org/wiki/Form_10-K) certification under  [ 404(a) ](https://www.cpajournal.com/2016/02/01/auditors-need-know-sox-section-404a-reports/) , therefore, 10-K - 404(a) certification can be submitted for Gitlab from FY 2022 (2nd year) onwards. 
*	Additionally, the external audit opinion on internal controls, which is required under [ sec 404(b) ](https://www.cpajournal.com/2016/02/01/auditors-need-know-sox-section-404a-reports/), is temporarily exempt for [ Emerging Growth Companies ](https://www.sec.gov/smallbusiness/goingpublic/EGC) (EGCs) for the first five years. While GitLab currently qualifies as an EGC, it is to be noted that this exemption is withdrawn if the criteria for EGCs are not satisfied in any year. 

Planned timelines for SOX Implementation:

<div align="center">
    <img src="/images/handbook/internal-audit/sarbanes-oxley_timeline.png" align="center" height="400" width="600" >
</div>
























